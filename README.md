# Wallhack radar implementations for Pwn Adventure 3
## Dependencies
The software has been developed and tested using **Linux Manjaro**. It requires the following system packages (the versions listed are verified to work):

- gcc 12.1.0
- protobuf 3.20.1
- python 3.10.4
- cmake 3.23.1

## Configuration, compilation and usage
For procuring game files and configuring your own server use [the official Pwn Adventure site](https://www.pwnadventure.com/) and publically available sources such as [this](https://github.com/beaujeant/PwnAdventure3/blob/master/INSTALL-server.md) or [this](https://github.com/LiveOverflow/PwnAdventure3).

### Code injection version

1. Resolve the dependencies & clone the repo.
2. If you want to display the radar using a different machine than the one your client is running on, change the `IP` value in `wallhack.cpp` so that it points at the machine displaying the radar.
3. If you want to use a different TCP port for data acquisition - presentation communication, change the `port` and `PORT` values in `wallhack.cpp` and `radar.py`. The two numbers must match exactly.
4. Compile the project using the `build.sh` helper script.
5. When running the game client, use environment variable `LD_PRELOAD` as indicated by the `build.sh` output.
6. If you want to detect NPCs as well, replace `wallhack.so` with `wallhack_npc.so` in the `LD_PRELOAD` value.
7. To get the readout, run the `radar.py` script that has been copied to the `build/` directory.
8. When you join the game, the wallhack will start operating automatically.

### Proxy version

1. Resolve the dependencies & clone the repo.
2. If you want to display the radar using a different machine than the one your proxy is running on, change the `PRESENTATION_HOST` value in `proxy.py` so that it points at the machine displaying the radar.
3. If you want to use a different TCP port for data acquisition - presentation communication, change the `PRESENTATION_PORT` and `PORT` values in `proxy.py` and `radar.py`. The two numbers must match exactly.
4. Alter the values `GAME_IP` (server IP), `MASTER_PORT` (master service port), `GAME_PORT_FIRST`, `GAME_PORT_LAST` (range of game room ports) in `proxy.py` so that they reflect the configuration of the server you're connecting to.
5. On the machine that is to run the game client, add entries in `/etc/hosts` for the master server (typically `master.pwn3`) and game server (typically `game.pwn3`) so that they point at the machine running the proxy or provide appropriate name resolution using your own DNS server.
6. Compile the project using the `build.sh` helper script.
7. Run the `proxy.py` script that has been copied to the `build/` directory and start your game client.
8. To get the readout, run the `radar.py` script that has been copied to the `build/` directory.
9. When you join the game, the wallhack will start operating automatically.

