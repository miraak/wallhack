#!/bin/env python

# The presentation layer implementation
import wallhack_pb2
import tkinter as tk
import socket
from threading import Lock, Thread
import sys
import os
import math

# Field of view & map size configuration
FOV=90
SCALE = 25

# Listening socket configuration: bind IP & port
HOST = '0.0.0.0'
PORT = 23496

contacts = []
mutex = Lock()
window = tk.Tk()
localPlayer = {}
residual = bytearray()

# Apply the transformation between game world coordinates and screen coordinates & check the show condition
def compute_contact(x, y, name):
    map_x = (x - localPlayer['x'])/SCALE
    map_y = (y - localPlayer['y'])/SCALE
    
    beta = localPlayer['rot']
    map_x_rot = map_x * math.cos(beta) - map_y * math.sin(beta)
    map_y_rot = map_x * math.sin(beta) + map_y * math.cos(beta)

    if(math.hypot(map_x_rot ,map_y_rot) < 250):
        contacts.append({'x': map_x_rot + 250, 'y': map_y_rot + 250})

# Parse a message from the data acquisition layer; if the packet does not contain a full
# Protocol Buffers message, wait for more data & try again
def parse(message):
    hack = wallhack_pb2.Hack()

    residual.extend(message)
    message = residual
    try:
        hack.ParseFromString(message)
    except:
        return

    residual.clear()

    # Print the data on stdout
    print(int(hack.timestamp/1000),
        hack.timestamp%1000,
        "LocalPlayer",
        hack.local.pos.x,
        hack.local.pos.y,
        hack.local.pos.z,
        hack.local.rot.pitch,
        hack.local.rot.yaw,
        hack.local.rot.roll, sep='; ',)
    for remote in hack.remote:
        print(int(hack.timestamp/1000),
            hack.timestamp%1000,
            remote.name,
            remote.pos.x,
            remote.pos.y,
            remote.pos.z,
            remote.rot.pitch,
            remote.rot.yaw,
            remote.rot.roll, sep='; ',)

    # Update local player data
    localPlayer['x'] = hack.local.pos.x
    localPlayer['y'] = hack.local.pos.y
    localPlayer['rot'] = math.radians((hack.local.rot.yaw + 90)*-1)

    # Update remote player table
    with mutex:   
        contacts.clear()
        for remote in hack.remote:
            compute_contact(remote.pos.x, remote.pos.y, remote.name)

# Open a listening socket, wait for the connection from the data acquisition layer,
# receive all messages & send for processing
def readerloop():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((HOST, PORT))
    while 1:
        print("Listening for incomming connections", file=sys.stderr)
        server.listen()
        peer, address = server.accept()
        print("Peer connected: ", address, file=sys.stderr)
        while 1:
            data = peer.recv(4096)
            if not data:
                break
            parse(data)   

# Draw a single enemy's dot on the radar
def add_contact(pins, canvas, pos_x, pos_y):
    pins.append(canvas.create_oval(pos_x-5, pos_y-5, pos_x+5, pos_y+5, fill='red', outline='red'))

# Clear the radar
def clear_contacts(pins, canvas):
    for c in pins:
        canvas.delete(c)

# Draw all enemie's dots on the radar
def draw(window, pins, canvas):
    clear_contacts(pins, canvas)
    with mutex:
        for c in contacts:
            add_contact(pins, canvas, c['x'], c['y'])
    # Register the next redraw event
    window.after(20, draw, window, pins, canvas)

# Stop the script after the radar is closed
def on_exit():
    window.destroy()
    os._exit(0)

def main():
    print("Radar started", file=sys.stderr)
    print("timestamp; milliseconds; name; x; y; z; pitch; yaw; roll")

    # Creata a Tkinter Canvas to draw the radar on
    canvas = tk.Canvas(window, height=500, width=500)
    canvas.pack()

    # Create the static background on the radar
    canvas.create_oval(0,0,500,500, fill='grey65', width = 2)
    canvas.create_arc(0, 0, 500, 500, start=90-FOV/2, extent=FOV, fill='grey85', width=2, style=tk.PIESLICE)
    for i in range(0, 200, 50):
        canvas.create_oval(0+i,0+i,500-i,500-i, width = 2)

    # Start the listening thread
    Thread(target=readerloop).start()

    pins = []

    # Register the exit handler
    window.protocol("WM_DELETE_WINDOW", on_exit)

    # Register the first redraw event
    window.after(20, draw, window, pins, canvas)

    # Use the main thread to control the radar window
    window.mainloop()

if __name__ == "__main__":
    main()