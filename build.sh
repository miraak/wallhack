#!/bin/bash

# Basic build script; creates, clears, configures & compiles the project
set -e

mkdir -p build
cd build

rm -rf *
cmake .. && make

echo "For data injection version use LD_PRELOAD=${PWD}/libwallhack.so when starting game."
