#!/bin/env python3

# The proxy-based data acquisition layer implementation
import wallhack_pb2
import socket
import select
from threading import Lock, Thread
import os
from enum import Enum
import struct
from time import sleep, time
import sys

# Presentation layer's socket address: IP & port
PRESENTATION_HOST = '127.0.0.1'
PRESENTATION_PORT = 23496

# Bind IP for proxy's listening sockets
BIND_IP = '0.0.0.0'

# Real server's IP's & ports
GAME_IP = '192.168.8.179'
MASTER_PORT = 3333
GAME_PORT_FIRST = 3000
GAME_PORT_LAST = 3005

class Direction(Enum):
	C2S = 1
	S2C = 2

# A simple testing stateless parser for printing data on stdout
class PrinterParser:
	def parse(self, data, direction, port):
		if direction is Direction.C2S:
			print("c->s: ", data)
		if direction is Direction.S2C:
			print("s->c: ", data)

# A class to manage the game state known to the cheat
# & to periodically push snapshots to the presentation layer
class PbOutput:
	def __init__(self):
		# The state is accessed by the parser and by the pushing thread, so a mutex is needed
		self.mutex = Lock()
		# Local player's (hacker's) position and rotation
		self.local = wallhack_pb2.Player()
		# Remote players' postions and rotations
		self.remote = {}

	def add_player(self, id, name):
		with self.mutex:
			self.remote[id] = wallhack_pb2.Player()
			self.remote[id].name = name
	
	def remove_player(self, id):
		with self.mutex:
			if id in self.remote:
				del self.remote[id]

	# Update local player's position
	def local_position(self, x, y, z, pitch, yaw, roll):
		with self.mutex:
			self.local.pos.x = x
			self.local.pos.y = y
			self.local.pos.z = z
			self.local.rot.pitch = pitch
			self.local.rot.yaw = yaw
			self.local.rot.roll = roll

	# Update remote player's postion
	def remote_position(self, id, x, y, z, pitch, yaw, roll):
		with self.mutex:
			if id in self.remote:
				self.remote[id].pos.x = x
				self.remote[id].pos.y = y
				self.remote[id].pos.z = z
				self.remote[id].rot.pitch = pitch
				self.remote[id].rot.yaw = yaw
				self.remote[id].rot.roll = roll

	def run(self):
		while 1:
			try:
				# Connect to the presentation layer
				sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
				sock.connect((PRESENTATION_HOST, PRESENTATION_PORT))
				message = wallhack_pb2.Hack()
				while 1:
					# Every 10 ms create & send new game state snapshot
					sleep(0.01)
					with self.mutex:
						message.local.CopyFrom(self.local)
						del message.remote[:]
						for player in self.remote:
							rem = message.remote.add()
							rem.CopyFrom(self.remote[player])
						message.timestamp = int(time()*1000)
						sock.sendall(message.SerializeToString())					
			except Exception as e:
				print('Exception: ', e, file=sys.stderr)
				sleep(0.01)
				continue					

# A stateful parser that decodes significant game state updates & saves them in a PbOutput object
class GameParser:
	def __init__(self, output):
		# Buffers to keep partial messages that are cut due to TCP characteristics
		self.data_s2c = bytearray()
		self.data_c2s = bytearray()
		# The parser can be accessed by multiple port threads when the player is moved between instances
		# a mutex is required
		self.mutex = Lock()
		self.output = output

	# Parsing entrypoint; differentiates between communication directions
	def parse(self, data, direction, port):
		with self.mutex:
			if direction is Direction.C2S:
				self.data_c2s.extend(data)
				self.decode_c2s()
			if direction is Direction.S2C:
				self.data_s2c.extend(data)
				self.decode_s2c()
	
	# Decode client to server packets
	def decode_c2s(self):
		while len(self.data_c2s) >= 2:
			# Local player moves
			if self.data_c2s[:2] == b'mv':
				if len(self.data_c2s) < 22:
					return
				# x, y, z, pitch, yaw, roll
				p = struct.unpack('=xxfffhhh', self.data_c2s[:20])
				self.output.local_position(p[0], p[1], p[2], 0, p[4] * 0.00549, 0)
				self.data_c2s = self.data_c2s[22:]
				continue

			# Process unknown bytes 1 by 1	
			self.data_c2s = self.data_c2s[1:]
	
	# Decode server to client packets
	def decode_s2c(self):
		while len(self.data_s2c) >= 2:
			# No updates
			if self.data_s2c[:2] == b'\x00\x00':
				self.data_s2c = self.data_s2c[2:]
				continue
				
			# NPC actor is added
			if self.data_s2c[:2] == b'mk':
				if len(self.data_s2c) < 13:
					return
				msg_len = struct.unpack('=xxixxxxxH',self.data_s2c[:13])[1] + 35
				if msg_len > len(self.data_s2c):
					return
				self.data_s2c = self.data_s2c[msg_len:]
				continue

			# New player joins (name & entity ID)
			if self.data_s2c[:2] == b'nc':
				if len(self.data_s2c) < 8:
					return
				data = struct.unpack('=xxIH', self.data_s2c[:8])
				id = data[0]
				name_len = data[1]
				if len(self.data_s2c) < 10 + name_len:
					return
				name = self.data_s2c[8:8 + name_len].decode('ascii')
				team_len = struct.unpack('=H', self.data_s2c[8 + name_len: 8 + name_len + 2])[0]
				if len(self.data_s2c) < 55 + name_len + team_len:
					return
				self.data_s2c = self.data_s2c[55 + name_len + team_len:]
				self.output.add_player(id, name)
				continue

			# Remote player moves
			if self.data_s2c[:2] == b'pp':
				if len(self.data_s2c) < 32:
					return
				data = struct.unpack('=xxIfff', self.data_s2c[:18])
				self.output.remote_position(data[0], data[1], data[2], data[3], 0, 0, 0)
				self.data_s2c = self.data_s2c[32:]
				continue

			# Remote player exits
			if self.data_s2c[:2] == b'^c':
				if len(self.data_s2c) < 8:
					return
				id = struct.unpack('=xxI', self.data_s2c[:6])[0]
				self.output.remove_player(id)
				self.data_s2c = self.data_s2c[8:]
				continue

			# Process unknown bytes 1 by 1
			self.data_s2c = self.data_s2c[1:]

	# Clear parser buffers when resetting for a new connection
	def clear(self):
		with self.mutex:
			self.data_s2c = bytearray()
			self.data_c2s = bytearray()

# The Proxy class impements the proxy behavior for 1 TCP port
class Proxy:
	def __init__(self, port, server_ip, parser):
		self.port = port
		self.server_ip = server_ip
		self.parser = parser
		self.client_connection = None

	# The entrypoint of the thread associated with the Proxy object: opens a listening
	# socket and waits for a connection		
	def run(self):
		self.client_connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# This property of the socket allows to use the same port right after resetting the script
		self.client_connection.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.client_connection.bind((BIND_IP, self.port))
		while 1:
			self.loop()
	
	def loop(self):
		if self.parser is not None:
			self.parser.clear()
		# Wait for a client to connect & accept the connection
		self.client_connection.listen()
		client, address = self.client_connection.accept()
		# Open the corresponding connection between the proxy and the game server
		server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		server.connect((self.server_ip, self.port))
		while 1:
			# Wait for the data to be available in either direction & send it a parser for processing
			readable, writable, errored = select.select([client, server], [], [])
			for sock in readable:
				try:
					if sock is client:
						data=client.recv(4096)
						if not data:
							return
						server.sendall(data)
						if self.parser is not None:
							self.parser.parse(data, Direction.C2S, self.port)
					if sock is server:
						data=server.recv(4096)
						if not data:
							return
						client.sendall(data)
						if self.parser is not None:
							self.parser.parse(data, Direction.S2C, self.port)
				except Exception as e:
					print('Exception: ', e, file=sys.stderr)
					return

def main():
	# The proxy for the master server connection; no parser selected
	# as no game state data is sent through this connection
	proxy_master = Proxy(MASTER_PORT, GAME_IP, None)
	Thread(target=proxy_master.run).start()
	
	# Prepare common output & parser objects for all the game server ports
	output = PbOutput()
	Thread(target=output.run).start()
	parser = GameParser(output)
	games = []
	# Open & start the proxy for each known game server port
	for p in range(GAME_PORT_FIRST, GAME_PORT_LAST):
		proxy_game = Proxy(p, GAME_IP, parser)		
		Thread(target=proxy_game.run).start()
		games.append(proxy_game)
	
	input("All threads started. Press Enter to terminate.")
	os._exit(0)
	
if __name__ == "__main__":
    main()
