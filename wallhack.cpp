/*
The code injection implementation of the data acquisition layer. The following source is used to compile a shared library
that can be injected into the game's process using the LD_PRELOAD environment variable.
*/

#include <iostream>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <chrono>

#include "libGameLogic.h"
#include "wallhack.pb.cc"

// Presentation layer's socket address: IP & port
const char * IP = "127.0.0.1";
const unsigned int port = 23496;

// The Protocol Buffer message object used for communication with the presentation layer
wallhack::Hack hack;
// The pre-prepared address structure to connect to the presentation layer
struct sockaddr_in address;
// The file descriptor's ID of the open socket to the presentation layer
int fd = -1;

// This is a special function that is executed when the shared object is loaded
static void constructor() __attribute__((constructor));
void constructor(){
    std::cerr<<"Wallhack shared library injected.\n";
    
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    address.sin_family = AF_INET;
    address.sin_port = htons(port);

    if(inet_pton(AF_INET, IP, &address.sin_addr) != 1){
        std::cerr<<"inet_pton(...) failed.\n";
        exit(1);
    }    
}

// This is a special function that is executed when the shared object is unloaded
static void destructor() __attribute__((destructor));
void destructor(){
    google::protobuf::ShutdownProtobufLibrary();

    if(fd != -1){
        close(fd);
    }

    std::cerr<<"Wallhack shared library unloaded.\n";
}

// A helper function to fill all fields of the Protocol Buffer message for one entity
void fill(wallhack::Player* message, Actor* actor, const char * name){
    Vector3 pos = actor->GetPosition();    
    message->mutable_pos()->set_x(pos.x);
    message->mutable_pos()->set_y(pos.y);
    message->mutable_pos()->set_z(pos.z);

    Rotation rot = actor->GetRotation();   
    message->mutable_rot()->set_pitch(rot.pitch);
    message->mutable_rot()->set_yaw(rot.yaw);
    message->mutable_rot()->set_roll(rot.roll);

    if(name != nullptr){
        message->set_name(name);
    }
}

// This World::Tick method implementation will be loaded before the original one
// and therefore will override it; the original implementation does not need to be called
// as its body is empty
void World::Tick(float f)
{
    // Get a pointer to the local player's object
    IPlayer* iplayer = (static_cast<ClientWorld*>(this))->m_activePlayer.m_object;
    Player* player = static_cast<Player*>(iplayer);

    // Clear the data from the previous pass
    hack.Clear();
    hack.set_timestamp(std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now().time_since_epoch()).count());

    // Add local player's position and rotation
    fill(hack.mutable_local(), player, nullptr);

// This implementation may be configured to map all actors (player's and NPCs) or just players
// in each case a different field is accessed and processed in a foreach manner
#ifdef WALLHACK_NPC
    for (ActorRef<IActor> _iactor : this->m_actors) {
        Actor* _actor = static_cast<Actor*>(_iactor.m_object);
        // Some actors are used for scripting within the game; they are usually invisible
        // and no interaction is possible; such entries are filtered out by looking at their displayed name
        if(strlen(_actor->GetDisplayName()) > 0){
            fill(hack.add_remote(), _actor, _actor->GetDisplayName());
        }
    }
#else
    for (ActorRef<IPlayer> _iplayer : this->m_players) {
        Actor* _actor = static_cast<Actor*>(_iplayer.m_object->GetActorInterface());
        if(!_iplayer.m_object->IsLocalPlayer()){
            fill(hack.add_remote(), _actor, _iplayer.m_object->GetPlayerName());
        }
    }
#endif

    // If the connection to the presentation layer does not exist, create it & connect
    if(fd == -1){
        fd = socket(AF_INET, SOCK_STREAM, 0);
        if(fd == -1){
            std::cerr << "socket(...) failed.\n";
            return;
        }

        if(connect(fd, (struct sockaddr*)&address, sizeof(address)) != 0){
            if(close(fd) != 0){
                std::cerr << "close(...) falied.\n";
                exit(2);
            }else{
                fd = -1;
                return;
            }
        }
    }

    // Serialize the data
    std::string packet;
    if(!hack.SerializeToString(&packet)){
        std::cerr << "SerializeToString(...) failed.\n";
        return;
    }

    // Send the data
    if(write(fd, packet.data(), packet.length()) != packet.length()){
        std::cerr << "write(...) failed. Closing.\n";
        if(close(fd) != 0){
            std::cerr << "close(...) falied.\n";
            exit(2);
        }else{
            fd = -1;
            return;
        }
    }
}
